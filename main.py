import pandas as pd

from core.email_sender import EmailSender
from core.html_modifier import modify_html_template

if __name__ == '__main__':
    my_email_sender = EmailSender('test_debug')

    pd_dict = [
        {'col1': 'value1', 'col2': 'another1'},
        {'col1': 'value2', 'col2': 'another2'},
        {'col1': 'value3', 'col2': 'another3'},
        {'col1': 'value4', 'col2': 'another4'},
    ]

    df = pd.DataFrame(pd_dict)

    df = df.style.set_properties(**{'border': '1px solid #1b1e24;',
                                    'tr:hover': 'red'})

    HTML = modify_html_template('base_mail_table',
                                message='tis is my message',
                                table=df.to_html()
                                )

    my_email_sender.sent_email(target=["one@email.com", "second@email.cz"],
                               subject='This is subject with HTML',
                               content=HTML,
                               html=True
                               )
