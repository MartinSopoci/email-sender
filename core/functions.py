import datetime


def get_current_timestamp() -> str:
    return datetime.datetime.now().strftime("%m/%d/%Y, %H:%M:%S")
