import logging
import smtplib
from email.message import EmailMessage

import yaml
from yaml import Loader

from core import constant
from core.logger_setup import initialize_logger

logger = logging.getLogger(constant.MODULE_NAME)


class EmailSender:
    def __init__(self, server_name):
        self.server_name = server_name
        with open(str(constant.CREDENTIALS_FILE_PATH), 'r') as yaml_file:
            self.smtp_server = yaml.load(yaml_file, Loader=Loader)[self.server_name]['smtp_server']

        initialize_logger()

    def sent_email(self, target, subject: str, content: str, html=False):
        if isinstance(target, (str, list)):
            if isinstance(target, str):
                target = [target]

            message = EmailMessage()

            message['Subject'] = subject
            message['From'] = self.smtp_server['username']
            message['To'] = target

            if html:
                message.add_alternative(content, subtype='html')
            else:
                message.set_content(content)

            try:
                with smtplib.SMTP_SSL(self.smtp_server['host'], self.smtp_server['port']) as smtp:
                    smtp.login(self.smtp_server['username'], self.smtp_server['password'])
                    smtp.send_message(message)

            except Exception as ex:
                logger.error(f"Sending mail failed on exception: {ex}")

        else:
            logger.warning(f"the target: {target} is of type: {type(target)} and not str/list.")
