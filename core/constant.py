from pathlib import Path

CREDENTIALS_FILE_PATH = Path('./core/credentials.yaml')
MODULE_NAME = 'email_sender'
LOG_FILE_PATH = Path('./email_sender.log')
HTML_TEMPLATE_FOLDER_PATH = Path('./core/html-templates/')
