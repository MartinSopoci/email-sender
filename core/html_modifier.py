from string import Template

from core.constant import HTML_TEMPLATE_FOLDER_PATH


def modify_html_template(template_name: str, **kwargs) -> str:
    full_template_path = HTML_TEMPLATE_FOLDER_PATH.joinpath(template_name).with_suffix('.html')

    with open(str(full_template_path), 'r') as base_mail_file:
        HTML = base_mail_file.read()

    return Template(HTML).safe_substitute(kwargs)
